1.0.0

Initial Release

Support Features:
--------------------------------------------

**BarChart功能介绍**

1) 添加图表数据
mBarChart.addBar();
1) 添加图表数据
mBarChart.addBarList();


**PieChart功能介绍**

1) 添加数据
mPieChart.addPieSlice(PieModel slice);
1) 获取当前选中的item
mPieChart.getCurrentItem();.
1) 设置选中的item
mPieChart.setCurrentItem();

**ValueLineChart功能介绍**

1) 添加图表数据
mValueLineChart.addSeries();
1) 获取图表数据
mValueLineChart.getDataSeries();

**StackedBarChart功能介绍**

1) 添加图表数据
mBarChart.addBar();
1) 添加图表数据
mBarChart.addBarList();


**BaseChart功能介绍**

1) 设置说明文字的颜色
mBarChart.setLegendColor();
1) 设置说明文字的高度
mBarChart.setLegendHeight();
1) 设置说明文字的字号
mBarChart.setLegendTextSize();
1) 设置动画的时间
mBarChart.setAnimationTime();
1) 设置文本
mBarChart.setEmptyDataText();
1) 获取是否将显示的提示转化为int类型
mBarChart.isShowDecimal();
1) 设置是否将显示的提示转化为int类型
mBarChart.setShowDecimal();
1) 获取图表的数据
mBarChart.getData();
1) 清除图表数据
mBarChart.clearChart();
1) 获取动画的时间
mBarChart.getAnimationTime();
1) 获取图表的条目间隔距离
mBarChart.getBarMargin();
1) 获取文字
mBarChart.getEmptyDataText();
1) 获取提示文字的颜色
mBarChart.getLegendColor();
1) 获取提示文字的高度
mBarChart.getLegendHeight();.
1) 获取提示文字的字体大小
mBarChart.getLegendTextSize();



**BaseBarChart功能介绍**


1) 设置每页显示的柱状个数
mBarChart.setVisibleBars();
1) 设置柱状图的边距
mBarChart.setBarMargin();
1) 设置柱状图每个条目的宽度
mBarChart.setBarWidth();
1) 获取图表的宽度
mBarChart.getBarWidth();
1) 获取图表的监听
mBarChart.getOnBarClickedListener();
1) 获取显示的条目个数
mBarChart.getVisibleBars();
1) 设置是否修复图表条目的宽度
mBarChart.setFixedBarWidth();
1) 设置图表的监听
mBarChart.setOnBarClickedListener();
1) 设置是否滚动
mBarChart.setScrollEnabled();
1) 设置滚动到最后
mBarChart.setScrollToEnd();
1) 设置是否显示值
mBarChart.setShowValues();




Not Support Features:
--------------------------------------------------------

1)  由于鸿蒙没有提供硬件加速的方法，所以图形的渲染不支持硬件加速
2) 由于使用的是从鸿蒙化来的手势识别器，相关的识别器的参数由于鸿蒙缺少ViewConfiguration中的相关方法，所以参数不支持动态获取
