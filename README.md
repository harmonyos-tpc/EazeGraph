该三方开源库从github fork过来，主要将底层接口调用的实现修改成鸿蒙接口的实现，将三方库鸿蒙化，供开发鸿蒙应用的开发者使用


fork版本号/日期：master / 2015/10/5


# EazeGraph

EazeGraph是一个用于创建精美图表库。它的主要目标是创建一个轻量级的库，该库易于使用并且高度可定制，具有“最新”外观。


项目移植状态：支持组件所有基本功能

完成度：100%

调用差异：无





## 导入方法

**har导入**

将har包放入lib文件夹并在build.gradle添加

```
implementation fileTree(dir: 'libs', include: ['*.har'])
```

**Library引用**

添加本工程中的模块到任意工程中，在需要使用的模块build.gradle中添加

```
compile project(path: ':EazeGraphLibrary')
```

or

```
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:EazeGraph:1.0.3'
```

## 使用方法
### Bar Chart
##### XML
```xml
<BarChart
            ohos:id="$+id:barchart"
            ohos:height="256vp"
            ohos:width="match_parent"
            ohos:egBarWidth="20vp"
            ohos:egFixedBarWidth="true"
            ohos:egLegendHeight="0vp"
            ohos:egShowDecimal="true"
            ohos:egShowValues="true"
            ohos:padding="10vp"/>
```

##### Java

```java
mBarChart = (BarChart) findComponentById(ResourceTable.Id_barchart);

mBarChart.addBar(new BarModel(2.3f, 0xFF123456));
mBarChart.addBar(new BarModel(2.f, 0xFF343456));
mBarChart.addBar(new BarModel(3.3f, 0xFF563456));
mBarChart.addBar(new BarModel(1.1f, 0xFF873F56));
mBarChart.addBar(new BarModel(2.7f, 0xFF56B7F1));
mBarChart.addBar(new BarModel(2.f, 0xFF343456));
mBarChart.addBar(new BarModel(0.4f, 0xFF1FF4AC));
mBarChart.addBar(new BarModel(1.0f, 0xFF1FF4AC));
mBarChart.addBar(new BarModel(0.5f, 0xFF1FF4AC));
mBarChart.addBar(new BarModel(4.f, 0xFF1BA4E6));
mBarChart.addBar(new BarModel(2.3f, 0xFF123456));
mBarChart.addBar(new BarModel(2.f, 0xFF343456));
mBarChart.addBar(new BarModel(3.3f, 0xFF563456));
mBarChart.addBar(new BarModel(1.1f, 0xFF873F56));
mBarChart.addBar(new BarModel(2.7f, 0xFF56B7F1));
mBarChart.addBar(new BarModel(2.f, 0xFF343456));
mBarChart.addBar(new BarModel(0.4f, 0xFF1FF4AC));
mBarChart.addBar(new BarModel(4.f, 0xFF1BA4E6));

mBarChart.startAnimation();

```

__________
### Stacked Bar Chart
 
##### XML

```xml
<StackedBarChart
        ohos:id="$+id:stackedbarchart"
        ohos:height="256vp"
        ohos:width="match_parent"
        ohos:egBarWidth="20vp"
        ohos:egEnableScroll="false"
        ohos:egFixedBarWidth="true"
        ohos:egLegendHeight="40vp"
        ohos:egShowSeparators="true"
        ohos:egShowValues="true"
        ohos:padding="10vp"/>

```

##### Java

```java
StackedBarModel s1 = new StackedBarModel("12.4");

s1.addBar(new BarModel(2.3f, 0xFF63CBB0));
s1.addBar(new BarModel(2.3f, 0xFF56B7F1));
s1.addBar(new BarModel(2.3f, 0xFFCDA67F));

StackedBarModel s2 = new StackedBarModel("13.4");
s2.addBar(new BarModel(1.1f, 0xFF63CBB0));
s2.addBar(new BarModel(2.7f, 0xFF56B7F1));
s2.addBar(new BarModel(0.7f, 0xFFCDA67F));

StackedBarModel s3 = new StackedBarModel("14.4");

s3.addBar(new BarModel(2.3f, 0xFF63CBB0));
s3.addBar(new BarModel(2.f, 0xFF56B7F1));
s3.addBar(new BarModel(3.3f, 0xFFCDA67F));

StackedBarModel s4 = new StackedBarModel("15.4");
s4.addBar(new BarModel(1.f, 0xFF63CBB0));
s4.addBar(new BarModel(4.2f, 0xFF56B7F1));
s4.addBar(new BarModel(2.1f, 0xFFCDA67F));

StackedBarModel s5 = new StackedBarModel("16.4");

s5.addBar(new BarModel(32.3f, 0xFF63CBB0));
s5.addBar(new BarModel(12.f, 0xFF56B7F1));
s5.addBar(new BarModel(22.3f, 0xFFCDA67F));

StackedBarModel s6 = new StackedBarModel("17.4");
s6.addBar(new BarModel(3.f, 0xFF63CBB0));
s6.addBar(new BarModel(.7f, 0xFF56B7F1));
s6.addBar(new BarModel(1.7f, 0xFFCDA67F));

StackedBarModel s7 = new StackedBarModel("18.4");

s7.addBar(new BarModel(2.3f, 0xFF63CBB0));
s7.addBar(new BarModel(2.f, 0xFF56B7F1));
s7.addBar(new BarModel(3.3f, 0xFFCDA67F));

StackedBarModel s8 = new StackedBarModel("19.4");
s8.addBar(new BarModel(5.4f, 0xFF63CBB0));
s8.addBar(new BarModel(2.7f, 0xFF56B7F1));
s8.addBar(new BarModel(3.4f, 0xFFCDA67F));

mStackedBarChart.addBar(s1);
mStackedBarChart.addBar(s2);
mStackedBarChart.addBar(s3);
mStackedBarChart.addBar(s4);
mStackedBarChart.addBar(s5);
mStackedBarChart.addBar(s6);
mStackedBarChart.addBar(s7);
mStackedBarChart.addBar(s8);

mStackedBarChart.startAnimation();

```

__________
### PieChart
 
##### XML

```xml
 <PieChart
         ohos:id="$+id:piechart"
         ohos:height="300vp"
         ohos:width="match_parent"
         ohos:egAutoCenter="true"
         ohos:egInnerValueUnit="%"
         ohos:egLegendTextSize="18fp"
         ohos:egOpenClockwise="false"
         ohos:egUsePieRotation="true"
         ohos:egValueTextSize="36fp"
         ohos:padding="10vp"/>

```


##### Java

```java
mPieChart = (PieChart) findComponentById(ResourceTable.Id_piechart);

mPieChart.addPieSlice(new PieModel("Freetime", 15, Color.getIntColor("#FE6DA8")));
mPieChart.addPieSlice(new PieModel("Sleep", 25, Color.getIntColor("#56B7F1")));
mPieChart.addPieSlice(new PieModel("Work", 35, Color.getIntColor("#CDA67F")));
mPieChart.addPieSlice(new PieModel("Eating", 9, Color.getIntColor("#FED70E")));

mPieChart.startAnimation();

```

__________
### Line Chart 
 
##### XML

```xml
<ValueLineChart
        ohos:id="$+id:linechart"
        ohos:height="200vp"
        ohos:width="match_parent"
        ohos:egActivateIndicatorShadow="false"
        ohos:egCurveSmoothness="0.4"
        ohos:egIndicatorLineColor="#FE6DA8"
        ohos:egIndicatorTextColor="#FE6DA8"
        ohos:egIndicatorTextUnit="g"
        ohos:egLegendHeight="40vp"
        ohos:egUseCubic="false"
        ohos:egUseDynamicScaling="false"
        ohos:egUseOverlapFill="true"/>

```

##### Java

```java
mValueLineChart = (ValueLineChart) findComponentById(ResourceTable.Id_linechart);
loadData();

ValueLineSeries series = new ValueLineSeries();
series.setColor(0xFF63CBB0);

series.addPoint(new ValueLinePoint(4.4f));
series.addPoint(new ValueLinePoint(2.4f));
series.addPoint(new ValueLinePoint(3.2f));
series.addPoint(new ValueLinePoint(2.6f));
series.addPoint(new ValueLinePoint(5.0f));
series.addPoint(new ValueLinePoint(3.5f));
series.addPoint(new ValueLinePoint(2.4f));
series.addPoint(new ValueLinePoint(0.4f));
series.addPoint(new ValueLinePoint(3.4f));
series.addPoint(new ValueLinePoint(2.5f));
series.addPoint(new ValueLinePoint(1.4f));
series.addPoint(new ValueLinePoint(4.4f));
series.addPoint(new ValueLinePoint(2.4f));
series.addPoint(new ValueLinePoint(3.2f));
series.addPoint(new ValueLinePoint(2.6f));
series.addPoint(new ValueLinePoint(5.0f));
series.addPoint(new ValueLinePoint(3.5f));
series.addPoint(new ValueLinePoint(2.4f));
series.addPoint(new ValueLinePoint(0.4f));
series.addPoint(new ValueLinePoint(3.4f));
series.addPoint(new ValueLinePoint(2.5f));
series.addPoint(new ValueLinePoint(1.0f));
series.addPoint(new ValueLinePoint(4.4f));
series.addPoint(new ValueLinePoint(2.4f));
series.addPoint(new ValueLinePoint(3.2f));
series.addPoint(new ValueLinePoint(2.6f));
series.addPoint(new ValueLinePoint(5.0f));
series.addPoint(new ValueLinePoint(3.5f));
series.addPoint(new ValueLinePoint(2.4f));
series.addPoint(new ValueLinePoint(0.4f));
series.addPoint(new ValueLinePoint(3.4f));
series.addPoint(new ValueLinePoint(2.5f));
series.addPoint(new ValueLinePoint(1.0f));
series.addPoint(new ValueLinePoint(4.2f));
series.addPoint(new ValueLinePoint(2.4f));
series.addPoint(new ValueLinePoint(3.6f));
series.addPoint(new ValueLinePoint(1.0f));
series.addPoint(new ValueLinePoint(2.5f));
series.addPoint(new ValueLinePoint(2.0f));
series.addPoint(new ValueLinePoint(1.4f));


mValueLineChart.addSeries(series);
mValueLineChart.startAnimation();

```
## 一些功能设置介绍


**BarChart功能介绍**

1) 添加图表数据
mBarChart.addBar();
1) 添加图表数据
mBarChart.addBarList();


**PieChart功能介绍**

1) 添加数据
mPieChart.addPieSlice(PieModel slice);
1) 获取当前选中的item
mPieChart.getCurrentItem();.
1) 设置选中的item
mPieChart.setCurrentItem();

**ValueLineChart功能介绍**

1) 添加图表数据
mValueLineChart.addSeries();
1) 获取图表数据
mValueLineChart.getDataSeries();

**StackedBarChart功能介绍**

1) 添加图表数据
mBarChart.addBar();
1) 添加图表数据
mBarChart.addBarList();


**BaseChart功能介绍**

1) 设置说明文字的颜色
mBarChart.setLegendColor();
1) 设置动画的时间
mBarChart.setAnimationTime();
1) 设置文本
mBarChart.setEmptyDataText();
1) 获取是否将显示的提示转化为int类型
mBarChart.isShowDecimal();
1) 设置是否将显示的提示转化为int类型
mBarChart.setShowDecimal();
1) 获取图表的数据
mBarChart.getData();
1) 清除图表数据
mBarChart.clearChart();
1) 获取动画的时间
mBarChart.getAnimationTime();
1) 获取图表的条目间隔距离
mBarChart.getBarMargin();
1) 获取文字
mBarChart.getEmptyDataText();
1) 获取提示文字的颜色
mBarChart.getLegendColor();
1) 获取提示文字的高度
mBarChart.getLegendHeight();.
1) 获取提示文字的字体大小
mBarChart.getLegendTextSize();



**BaseBarChart功能介绍**


1) 设置每页显示的柱状个数
mBarChart.setVisibleBars();
1) 设置柱状图的边距
mBarChart.setBarMargin();
1) 设置柱状图每个条目的宽度
mBarChart.setBarWidth();
1) 获取图表的宽度
mBarChart.getBarWidth();
1) 获取图表的监听
mBarChart.getOnBarClickedListener();
1) 获取显示的条目个数
mBarChart.getVisibleBars();
1) 设置是否修复图表条目的宽度
mBarChart.setFixedBarWidth();
1) 设置图表的监听
mBarChart.setOnBarClickedListener();
1) 设置是否滚动
mBarChart.setScrollEnabled();
1) 设置滚动到最后
mBarChart.setScrollToEnd();
1) 设置是否显示值
mBarChart.setShowValues();

