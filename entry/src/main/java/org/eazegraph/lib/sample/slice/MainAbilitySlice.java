package org.eazegraph.lib.sample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;
import org.eazegraph.lib.charts.BarChart;
import org.eazegraph.lib.charts.BaseBarChart;
import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.communication.IOnItemFocusChangedListener;
import org.eazegraph.lib.models.BarModel;
import org.eazegraph.lib.models.PieModel;
import org.eazegraph.lib.sample.ResourceTable;

/**
 * Main ability slice
 */
public class MainAbilitySlice extends AbilitySlice {
    /**
     * The constant Base bar chart
     */
    private BaseBarChart baseBarChart;
    /**
     * The constant M pie chart
     */
    private PieChart mPieChart;
    /**
     * The constant M bar chart
     */
    private BarChart mBarChart;

    /**
     * On start *
     *
     * @param intent intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        mBarChart = (BarChart) findComponentById(ResourceTable.Id_barchart);
        loadData();
        mBarChart.startAnimation();

        mPieChart = (PieChart) findComponentById(ResourceTable.Id_piechart);
        loadPieChartData();
        mPieChart.startAnimation();
    }

    /**
     * Load pie chart data
     */
    private void loadPieChartData() {
        mPieChart.addPieSlice(new PieModel("Freetime", 15, Color.getIntColor("#FE6DA8")));
        mPieChart.addPieSlice(new PieModel("Sleep", 25, Color.getIntColor("#56B7F1")));
        mPieChart.addPieSlice(new PieModel("Work", 35, Color.getIntColor("#CDA67F")));
        mPieChart.addPieSlice(new PieModel("Eating", 9, Color.getIntColor("#FED70E")));

        mPieChart.setOnItemFocusChangedListener(new IOnItemFocusChangedListener() {
            @Override
            public void onItemFocusChanged(int _Position) {
            }
        });
    }

    /**
     * Load data
     */
    private void loadData() {
        mBarChart.addBar(new BarModel(2.3f, 0xFF123456));
        mBarChart.addBar(new BarModel(2.f, 0xFF343456));
        mBarChart.addBar(new BarModel(3.3f, 0xFF563456));
        mBarChart.addBar(new BarModel(1.1f, 0xFF873F56));
        mBarChart.addBar(new BarModel(2.7f, 0xFF56B7F1));
        mBarChart.addBar(new BarModel(2.f, 0xFF343456));
        mBarChart.addBar(new BarModel(0.4f, 0xFF1FF4AC));
        mBarChart.addBar(new BarModel(1.0f, 0xFF1FF4AC));
        mBarChart.addBar(new BarModel(0.5f, 0xFF1FF4AC));
        mBarChart.addBar(new BarModel(4.f, 0xFF1BA4E6));
        mBarChart.addBar(new BarModel(2.3f, 0xFF123456));
        mBarChart.addBar(new BarModel(2.f, 0xFF343456));
        mBarChart.addBar(new BarModel(3.3f, 0xFF563456));
        mBarChart.addBar(new BarModel(1.1f, 0xFF873F56));
        mBarChart.addBar(new BarModel(2.7f, 0xFF56B7F1));
        mBarChart.addBar(new BarModel(2.f, 0xFF343456));
        mBarChart.addBar(new BarModel(0.4f, 0xFF1FF4AC));
        mBarChart.addBar(new BarModel(4.f, 0xFF1BA4E6));
    }

    /**
     * On active
     */
    @Override
    public void onActive() {
        super.onActive();
    }

    /**
     * On foreground *
     *
     * @param intent intent
     */
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
