package org.eazegraph.lib.sample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.ListDialog;
import org.eazegraph.lib.charts.ValueLineChart;
import org.eazegraph.lib.communication.IOnPointFocusedListener;
import org.eazegraph.lib.sample.ResourceTable;
import org.eazegraph.lib.models.ValueLinePoint;
import org.eazegraph.lib.models.ValueLineSeries;
import org.eazegraph.lib.utils.LogUtil;

/**
 * Cubic value line ability slice
 */
public class CubicValueLineAbilitySlice extends AbilitySlice {
    /**
     * The constant M cubic value line chart
     */
    private ValueLineChart mCubicValueLineChart;

    /**
     * On start *
     *
     * @param intent intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_cubic_valueline);
        mCubicValueLineChart = (ValueLineChart) findComponentById(ResourceTable.Id_cubiclinechart);
        loadData();
        mCubicValueLineChart.startAnimation();
        initChangeView();
        initRestart();
    }

    /**
     * Init restart
     */
    private void initRestart() {
        findComponentById(ResourceTable.Id_btn_restart).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mCubicValueLineChart.startAnimation();
            }
        });
    }

    /**
     * Init change view
     */
    private void initChangeView() {
        String[] item = new String[]{"Bar Chart", "Stacked Bar Chart", "Pie Chart", "Line Chart", "Cubic Line Chart", "Vertical Bar Chart"};
        findComponentById(ResourceTable.Id_change_view).setClickedListener(new Component.ClickedListener() {
            void gotoAbility(int position) {
                switch (position) {
                    case 0:
                        present(new BarChartAbilitySlice(), new Intent());
                        break;
                    case 1:
                        present(new StackedBarChartAbilitySlice(), new Intent());
                        break;
                    case 2:
                        present(new PieChartAbilitySlice(), new Intent());
                        break;
                    case 3:
                        present(new ValueLineChartAbilitySlice(), new Intent());
                        break;
                    case 4:
                        present(new CubicValueLineAbilitySlice(), new Intent());
                        break;
                    case 5:
                        present(new VerticalBarChartAbilitySlice(), new Intent());
                        break;
                }
            }

            @Override
            public void onClick(Component component) {
                ListDialog listDialog = new ListDialog(getContext());
                listDialog.setSingleSelectItems(item, 4);
                listDialog.setOnSingleSelectListener(new IDialog.ClickedListener() {
                    @Override
                    public void onClick(IDialog iDialog, int position) {
                        gotoAbility(position);
                        listDialog.hide();
                    }
                });
                listDialog.setButton(0, "取消", new IDialog.ClickedListener() {
                    @Override
                    public void onClick(IDialog iDialog, int position) {
                        listDialog.destroy();
                    }
                });
                if (!getAbility().isTerminating() && !listDialog.isShowing()) {
                    listDialog.show();
                }
            }
        });
    }

    /**
     * Load data
     */
    private void loadData() {
        ValueLineSeries series = new ValueLineSeries();
        series.setColor(0xFF56B7F1);

        series.addPoint(new ValueLinePoint("Jan", 2.4f));
        series.addPoint(new ValueLinePoint("Feb", 3.4f));
        series.addPoint(new ValueLinePoint("Mar", .4f));
        series.addPoint(new ValueLinePoint("Apr", 1.2f));
        series.addPoint(new ValueLinePoint("Mai", 2.6f));
        series.addPoint(new ValueLinePoint("Jun", -1.0f));
        series.addPoint(new ValueLinePoint("Jul", 3.5f));
        series.addPoint(new ValueLinePoint("Aug", 2.4f));
        series.addPoint(new ValueLinePoint("Sep", 2.4f));
        series.addPoint(new ValueLinePoint("Oct", 3.4f));
        series.addPoint(new ValueLinePoint("Nov", -.4f));
        series.addPoint(new ValueLinePoint("Dec", -1.0f));
        series.addPoint(new ValueLinePoint("Jan", 1.2f));
        series.addPoint(new ValueLinePoint("Feb", 3.4f));
        series.addPoint(new ValueLinePoint("Mar", -2.0f));
        series.addPoint(new ValueLinePoint("Apr", -1.0f));
        series.addPoint(new ValueLinePoint("Mai", 3.5f));
        series.addPoint(new ValueLinePoint("Jun", 2.4f));
        series.addPoint(new ValueLinePoint("Jan", 2.4f));
        series.addPoint(new ValueLinePoint("Feb", 3.4f));

        mCubicValueLineChart.addSeries(series);
        mCubicValueLineChart.addStandardValue(2.3f);
        mCubicValueLineChart.addStandardValue(-1.3f);
        mCubicValueLineChart.addStandardValue(4.3f);
        mCubicValueLineChart.setOnPointFocusedListener(new IOnPointFocusedListener() {
            @Override
            public void onPointFocused(int _PointPos) {
                LogUtil.info("Test", "Pos: " + _PointPos);
            }
        });
    }

    /**
     * On active
     */
    @Override
    public void onActive() {
        super.onActive();
    }

    /**
     * On foreground *
     *
     * @param intent intent
     */
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
