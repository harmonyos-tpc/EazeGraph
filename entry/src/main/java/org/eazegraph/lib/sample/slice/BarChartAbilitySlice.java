package org.eazegraph.lib.sample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.ListDialog;
import org.eazegraph.lib.charts.BarChart;
import org.eazegraph.lib.charts.BaseBarChart;
import org.eazegraph.lib.models.BarModel;
import org.eazegraph.lib.sample.ResourceTable;

/**
 * Bar chart ability slice
 */
public class BarChartAbilitySlice extends AbilitySlice {
    /**
     * The constant Base bar chart
     */
    private BaseBarChart baseBarChart;
    /**
     * The constant M bar chart
     */
    private BarChart mBarChart;

    /**
     * On start *
     *
     * @param intent intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_barchart);
        mBarChart = (BarChart) findComponentById(ResourceTable.Id_barchart);
        loadData();
        mBarChart.startAnimation();
        initChangeView();
        initRestart();
    }

    /**
     * Init restart
     */
    private void initRestart() {
        findComponentById(ResourceTable.Id_btn_restart).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mBarChart.startAnimation();
            }
        });
    }

    /**
     * Init change view
     */
    private void initChangeView() {
        String[] item = new String[]{"Bar Chart", "Stacked Bar Chart", "Pie Chart", "Line Chart", "Cubic Line Chart", "Vertical Bar Chart"};
        findComponentById(ResourceTable.Id_change_view).setClickedListener(new Component.ClickedListener() {
            void gotoAbility(int position) {
                switch (position) {
                    case 0:
                        present(new BarChartAbilitySlice(), new Intent());
                        break;
                    case 1:
                        present(new StackedBarChartAbilitySlice(), new Intent());
                        break;
                    case 2:
                        present(new PieChartAbilitySlice(), new Intent());
                        break;
                    case 3:
                        present(new ValueLineChartAbilitySlice(), new Intent());
                        break;
                    case 4:
                        present(new CubicValueLineAbilitySlice(), new Intent());
                        break;
                    case 5:
                        present(new VerticalBarChartAbilitySlice(), new Intent());
                        break;
                }
            }

            @Override
            public void onClick(Component component) {
                ListDialog listDialog = new ListDialog(getContext());
                listDialog.setSingleSelectItems(item, 0);
                listDialog.setOnSingleSelectListener(new IDialog.ClickedListener() {
                    @Override
                    public void onClick(IDialog iDialog, int position) {
                        gotoAbility(position);
                        listDialog.hide();
                    }
                });
                listDialog.setButton(0, "取消", new IDialog.ClickedListener() {
                    @Override
                    public void onClick(IDialog iDialog, int position) {
                        listDialog.destroy();
                    }
                });
                if (!getAbility().isTerminating() && !listDialog.isShowing()) {
                    listDialog.show();
                }
            }
        });
    }

    /**
     * Load data
     */
    private void loadData() {
        mBarChart.addBar(new BarModel(2.3f, 0xFF123456));
        mBarChart.addBar(new BarModel(2.f, 0xFF343456));
        mBarChart.addBar(new BarModel(3.3f, 0xFF563456));
        mBarChart.addBar(new BarModel(1.1f, 0xFF873F56));
        mBarChart.addBar(new BarModel(2.7f, 0xFF56B7F1));
        mBarChart.addBar(new BarModel(2.f, 0xFF343456));
        mBarChart.addBar(new BarModel(0.4f, 0xFF1FF4AC));
        mBarChart.addBar(new BarModel(1.0f, 0xFF1FF4AC));
        mBarChart.addBar(new BarModel(0.5f, 0xFF1FF4AC));
        mBarChart.addBar(new BarModel(4.f, 0xFF1BA4E6));
        mBarChart.addBar(new BarModel(2.3f, 0xFF123456));
        mBarChart.addBar(new BarModel(2.f, 0xFF343456));
        mBarChart.addBar(new BarModel(3.3f, 0xFF563456));
        mBarChart.addBar(new BarModel(1.1f, 0xFF873F56));
        mBarChart.addBar(new BarModel(2.7f, 0xFF56B7F1));
        mBarChart.addBar(new BarModel(2.f, 0xFF343456));
        mBarChart.addBar(new BarModel(0.4f, 0xFF1FF4AC));
        mBarChart.addBar(new BarModel(4.f, 0xFF1BA4E6));
    }

    /**
     * On active
     */
    @Override
    public void onActive() {
        super.onActive();
    }

    /**
     * On foreground *
     *
     * @param intent intent
     */
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
