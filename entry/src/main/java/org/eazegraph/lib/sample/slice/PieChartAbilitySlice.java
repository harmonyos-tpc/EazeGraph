package org.eazegraph.lib.sample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.ListDialog;
import org.eazegraph.lib.charts.BaseBarChart;
import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.communication.IOnItemFocusChangedListener;
import org.eazegraph.lib.sample.ResourceTable;
import org.eazegraph.lib.models.PieModel;

/**
 * Pie chart ability slice
 */
public class PieChartAbilitySlice extends AbilitySlice {
    /**
     * The constant Base bar chart
     */
    private BaseBarChart baseBarChart;
    /**
     * The constant M pie chart
     */
    private PieChart mPieChart;

    /**
     * On start *
     *
     * @param intent intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_piechart);
        mPieChart = (PieChart) findComponentById(ResourceTable.Id_piechart);
        loadPieChartData();
        mPieChart.startAnimation();
        initChangeView();
        initRestart();
    }

    /**
     * Init restart
     */
    private void initRestart() {
        findComponentById(ResourceTable.Id_btn_restart).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mPieChart.startAnimation();
            }
        });
    }

    /**
     * Init change view
     */
    private void initChangeView() {
        String[] item = new String[]{"Bar Chart", "Stacked Bar Chart", "Pie Chart", "Line Chart", "Cubic Line Chart", "Vertical Bar Chart"};
        findComponentById(ResourceTable.Id_change_view).setClickedListener(new Component.ClickedListener() {
            void gotoAbility(int position) {
                switch (position) {
                    case 0:
                        present(new BarChartAbilitySlice(), new Intent());
                        break;
                    case 1:
                        present(new StackedBarChartAbilitySlice(), new Intent());
                        break;
                    case 2:
                        present(new PieChartAbilitySlice(), new Intent());
                        break;
                    case 3:
                        present(new ValueLineChartAbilitySlice(), new Intent());
                        break;
                    case 4:
                        present(new CubicValueLineAbilitySlice(), new Intent());
                        break;
                    case 5:
                        present(new VerticalBarChartAbilitySlice(), new Intent());
                        break;
                }
            }

            @Override
            public void onClick(Component component) {
                ListDialog listDialog = new ListDialog(getContext());
                listDialog.setSingleSelectItems(item, 2);
                listDialog.setOnSingleSelectListener(new IDialog.ClickedListener() {
                    @Override
                    public void onClick(IDialog iDialog, int position) {
                        gotoAbility(position);
                        listDialog.hide();
                    }
                });
                listDialog.setButton(0, "取消", new IDialog.ClickedListener() {
                    @Override
                    public void onClick(IDialog iDialog, int position) {
                        listDialog.destroy();
                    }
                });
                if (!getAbility().isTerminating() && !listDialog.isShowing()) {
                    listDialog.show();
                }
            }
        });
    }

    /**
     * Load pie chart data
     */
    private void loadPieChartData() {
        mPieChart.addPieSlice(new PieModel("Freetime", 15, Color.getIntColor("#FE6DA8")));
        mPieChart.addPieSlice(new PieModel("Sleep", 25, Color.getIntColor("#56B7F1")));
        mPieChart.addPieSlice(new PieModel("Work", 35, Color.getIntColor("#CDA67F")));
        mPieChart.addPieSlice(new PieModel("Eating", 9, Color.getIntColor("#FED70E")));

        mPieChart.setOnItemFocusChangedListener(new IOnItemFocusChangedListener() {
            @Override
            public void onItemFocusChanged(int _Position) {
            }
        });
    }

    /**
     * On active
     */
    @Override
    public void onActive() {
        super.onActive();
    }

    /**
     * On foreground *
     *
     * @param intent intent
     */
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
