package org.eazegraph.lib.sample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.ListDialog;
import org.eazegraph.lib.charts.BaseBarChart;
import org.eazegraph.lib.charts.StackedBarChart;
import org.eazegraph.lib.models.BarModel;
import org.eazegraph.lib.models.StackedBarModel;
import org.eazegraph.lib.sample.ResourceTable;

/**
 * Stacked bar chart ability slice
 */
public class StackedBarChartAbilitySlice extends AbilitySlice {
    /**
     * The constant Base bar chart
     */
    private BaseBarChart baseBarChart;
    /**
     * The constant M stacked bar chart
     */
    private StackedBarChart mStackedBarChart;

    /**
     * On start *
     *
     * @param intent intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_stack_barchart);
        mStackedBarChart = (StackedBarChart) findComponentById(ResourceTable.Id_stackedbarchart);
        loadData();
        mStackedBarChart.startAnimation();
        initChangeView();
        initRestart();
    }

    /**
     * Init restart
     */
    private void initRestart() {
        findComponentById(ResourceTable.Id_btn_restart).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mStackedBarChart.startAnimation();
            }
        });
    }

    /**
     * Init change view
     */
    private void initChangeView() {
        String[] item = new String[]{"Bar Chart", "Stacked Bar Chart", "Pie Chart", "Line Chart", "Cubic Line Chart", "Vertical Bar Chart"};
        findComponentById(ResourceTable.Id_change_view).setClickedListener(new Component.ClickedListener() {
            void gotoAbility(int position) {
                switch (position) {
                    case 0:
                        present(new BarChartAbilitySlice(), new Intent());
                        break;
                    case 1:
                        present(new StackedBarChartAbilitySlice(), new Intent());
                        break;
                    case 2:
                        present(new PieChartAbilitySlice(), new Intent());
                        break;
                    case 3:
                        present(new ValueLineChartAbilitySlice(), new Intent());
                        break;
                    case 4:
                        present(new CubicValueLineAbilitySlice(), new Intent());
                        break;
                    case 5:
                        present(new VerticalBarChartAbilitySlice(), new Intent());
                        break;
                }
            }

            @Override
            public void onClick(Component component) {
                ListDialog listDialog = new ListDialog(getContext());
                listDialog.setSingleSelectItems(item, 1);
                listDialog.setOnSingleSelectListener(new IDialog.ClickedListener() {
                    @Override
                    public void onClick(IDialog iDialog, int position) {
                        gotoAbility(position);
                        listDialog.hide();
                    }
                });
                listDialog.setButton(0, "取消", new IDialog.ClickedListener() {
                    @Override
                    public void onClick(IDialog iDialog, int position) {
                        listDialog.destroy();
                    }
                });
                if (!getAbility().isTerminating() && !listDialog.isShowing()) {
                    listDialog.show();
                }
            }
        });
    }

    /**
     * Load data
     */
    private void loadData() {
        StackedBarModel s1 = new StackedBarModel("12.4");

        s1.addBar(new BarModel(2.3f, 0xFF63CBB0));
        s1.addBar(new BarModel(2.3f, 0xFF56B7F1));
        s1.addBar(new BarModel(2.3f, 0xFFCDA67F));

        StackedBarModel s2 = new StackedBarModel("13.4");
        s2.addBar(new BarModel(1.1f, 0xFF63CBB0));
        s2.addBar(new BarModel(2.7f, 0xFF56B7F1));
        s2.addBar(new BarModel(0.7f, 0xFFCDA67F));

        StackedBarModel s3 = new StackedBarModel("14.4");

        s3.addBar(new BarModel(2.3f, 0xFF63CBB0));
        s3.addBar(new BarModel(2.f, 0xFF56B7F1));
        s3.addBar(new BarModel(3.3f, 0xFFCDA67F));

        StackedBarModel s4 = new StackedBarModel("15.4");
        s4.addBar(new BarModel(1.f, 0xFF63CBB0));
        s4.addBar(new BarModel(4.2f, 0xFF56B7F1));
        s4.addBar(new BarModel(2.1f, 0xFFCDA67F));

        StackedBarModel s5 = new StackedBarModel("16.4");

        s5.addBar(new BarModel(32.3f, 0xFF63CBB0));
        s5.addBar(new BarModel(12.f, 0xFF56B7F1));
        s5.addBar(new BarModel(22.3f, 0xFFCDA67F));

        StackedBarModel s6 = new StackedBarModel("17.4");
        s6.addBar(new BarModel(3.f, 0xFF63CBB0));
        s6.addBar(new BarModel(.7f, 0xFF56B7F1));
        s6.addBar(new BarModel(1.7f, 0xFFCDA67F));

        StackedBarModel s7 = new StackedBarModel("18.4");

        s7.addBar(new BarModel(2.3f, 0xFF63CBB0));
        s7.addBar(new BarModel(2.f, 0xFF56B7F1));
        s7.addBar(new BarModel(3.3f, 0xFFCDA67F));

        StackedBarModel s8 = new StackedBarModel("19.4");
        s8.addBar(new BarModel(5.4f, 0xFF63CBB0));
        s8.addBar(new BarModel(2.7f, 0xFF56B7F1));
        s8.addBar(new BarModel(3.4f, 0xFFCDA67F));

        mStackedBarChart.addBar(s1);
        mStackedBarChart.addBar(s2);
        mStackedBarChart.addBar(s3);
        mStackedBarChart.addBar(s4);
        mStackedBarChart.addBar(s5);
        mStackedBarChart.addBar(s6);
        mStackedBarChart.addBar(s7);
        mStackedBarChart.addBar(s8);
    }

    /**
     * On active
     */
    @Override
    public void onActive() {
        super.onActive();
    }

    /**
     * On foreground *
     *
     * @param intent intent
     */
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
