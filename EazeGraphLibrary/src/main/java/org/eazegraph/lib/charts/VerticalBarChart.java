/**
 * Copyright (C) 2014 Paul Cech
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.eazegraph.lib.charts;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;
import org.eazegraph.lib.models.BarModel;
import org.eazegraph.lib.models.BaseModel;
import org.eazegraph.lib.utils.AttrUtils;
import org.eazegraph.lib.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple Bar Chart where the bar heights are dependent on each other.
 */
public class VerticalBarChart extends BaseBarChart {
    /**
     * * The constant DEF_USE_MAXIMUM_VALUE
     */
    public static final boolean DEF_USE_MAXIMUM_VALUE = false;
    /**
     * * The constant DEF_MAXIMUM_VALUE
     */
    public static final float DEF_MAXIMUM_VALUE = 150.0f;
    /**
     * * The constant DEF_VALUE_UNIT
     */
    public static final String DEF_VALUE_UNIT = "";
    /**
     * * The constant DEF_SHOW_BAR_LABEL
     */
    public static final boolean DEF_SHOW_BAR_LABEL = false;
    /**
     * * The constant DEF_BAR_LABEL_COLOR
     */
    public static final int DEF_BAR_LABEL_COLOR = 0xFF898989;
    /**
     * * The constant LOG_TAG
     */
    private static final String LOG_TAG = VerticalBarChart.class.getSimpleName();
    /**
     * Constructor that is called when inflating a view from XML. This is called
     * when a view is being constructed from an XML file, supplying attributes
     * that were specified in the XML file. This version uses a default style of
     * 0, so the only attribute values applied are those in the Context's Theme
     * and the given AttributeSet.
     * <p/>
     * <p/>
     * The method onFinishInflate() will be called after all children have been
     * added.
     *
     * @param context The Context the view is running in, through which it can                access the current theme, resources, etc.
     * @param attrs The attributes of the XML tag that is inflating the view.
     */
    private final String VerticalBarChart_egUseMaximumValue = "egUseMaximumValue";
    /**
     * * The constant VerticalBarChart_egMaximumValue
     */
    private final String VerticalBarChart_egMaximumValue = "egMaximumValue";
    /**
     * * The constant VerticalBarChart_egValueUnit
     */
    private final String VerticalBarChart_egValueUnit = "egValueUnit";
    /**
     * * The constant VerticalBarChart_egShowBarLabel
     */
    private final String VerticalBarChart_egShowBarLabel = "egShowBarLabel";
    /**
     * * The constant VerticalBarChart_egBarLabelColor
     */
    private final String VerticalBarChart_egBarLabelColor = "egBarLabelColor";
    /**
     * The constant M data
     */
    private List<BarModel> mData;
    /**
     * The constant M value paint
     */
    private Paint mValuePaint;
    /**
     * The constant M maximum value
     */
    private float mMaximumValue;
    /**
     * The constant M use maximum value
     */
    private boolean mUseMaximumValue;
    /**
     * The constant M value unit
     */
    private String mValueUnit;
    /**
     * The constant M show bar label
     */
    private boolean mShowBarLabel;
    /**
     * The constant M bar label color
     */
    private int mBarLabelColor;
    /**
     * The constant M value distance
     */
    private int mValueDistance = (int) Utils.dpToPx(getContext(),4);

    /**
     * Simple constructor to use when creating a view from code.
     *
     * @param context The Context the view is running in, through which it can                access the current theme, resources, etc.
     */
    public VerticalBarChart(Context context) {
        super(context);

        mUseMaximumValue = DEF_USE_MAXIMUM_VALUE;
        mMaximumValue = DEF_MAXIMUM_VALUE;
        mValueUnit = DEF_VALUE_UNIT;
        mShowBarLabel = DEF_SHOW_BAR_LABEL;
        mBarLabelColor = DEF_BAR_LABEL_COLOR;

        initializeGraph();
    }

    /**
     * Vertical bar chart
     *
     * @param context context
     * @param attrs   attrs
     */
    public VerticalBarChart(Context context, AttrSet attrs) {
        super(context, attrs);

        mUseMaximumValue = AttrUtils.getBooleanFromAttr(attrs, VerticalBarChart_egUseMaximumValue, DEF_USE_MAXIMUM_VALUE);
        mMaximumValue = AttrUtils.getFloatFromAttr(attrs, VerticalBarChart_egMaximumValue, DEF_MAXIMUM_VALUE);
        mValueUnit = AttrUtils.getStringFromAttr(attrs, VerticalBarChart_egValueUnit, "");
        mShowBarLabel = AttrUtils.getBooleanFromAttr(attrs, VerticalBarChart_egShowBarLabel, DEF_SHOW_BAR_LABEL);
        mBarLabelColor = AttrUtils.getColorFromAttr(attrs, VerticalBarChart_egBarLabelColor, DEF_BAR_LABEL_COLOR);

        if (mValueUnit == null) {
            mValueUnit = DEF_VALUE_UNIT;
        }

        initializeGraph();
    }

    /**
     * Adds a new {@link BarModel} to the BarChart.
     *
     * @param _Bar The BarModel which will be added to the chart.
     */
    public void addBar(BarModel _Bar) {
        if (_Bar == null) {
            return;
        }        
        mData.add(_Bar);
        onDataChanged();
    }

    /**
     * Adds a new list of {@link BarModel} to the BarChart.
     *
     * @param _List The BarModel list which will be added to the chart.
     */
    public void addBarList(List<BarModel> _List) {
        if (_List == null) {
            return;
        }
        mData = _List;
        onDataChanged();
    }

    /**
     * Returns the data which is currently present in the chart.
     *
     * @return The currently used data.
     */
    @Override
    public List<BarModel> getData() {
        return mData;
    }

    /**
     * Get maximum value float
     *
     * @return float
     */
    public float getMaximumValue() {
        return mMaximumValue;
    }

    /**
     * Set maximum value *
     *
     * @param _maximumValue maximum value
     */
    public void setMaximumValue(float _maximumValue) {
        mMaximumValue = _maximumValue;
        onDataChanged();
    }

    /**
     * Is use maximum value boolean
     *
     * @return boolean
     */
    public boolean isUseMaximumValue() {
        return mUseMaximumValue;
    }

    /**
     * Set use maximum value *
     *
     * @param _useMaximumValue use maximum value
     */
    public void setUseMaximumValue(boolean _useMaximumValue) {
        mUseMaximumValue = _useMaximumValue;
        onDataChanged();
    }

    /**
     * Get value unit string
     *
     * @return string
     */
    public String getValueUnit() {
        return mValueUnit;
    }

    /**
     * Set value unit *
     *
     * @param _valueUnit value unit
     */
    public void setValueUnit(String _valueUnit) {
        mValueUnit = _valueUnit == null ? DEF_VALUE_UNIT : _valueUnit;
    }

    /**
     * Get bar label boolean
     *
     * @return boolean
     */
    public boolean getBarLabel() {
        return mShowBarLabel;
    }

    /**
     * Set bar label *
     *
     * @param _showBarLabel show bar label
     */
    public void setBarLabel(boolean _showBarLabel) {
        mShowBarLabel = _showBarLabel;
        onDataChanged();
    }

    /**
     * Get bar label color int
     *
     * @return int
     */
    public int getBarLabelColor() {
        return mBarLabelColor;
    }

    /**
     * Set bar label color *
     *
     * @param _barLabelColor bar label color
     */
    public void setBarLabelColor(int _barLabelColor) {
        mBarLabelColor = _barLabelColor;
        invalidateGraph();
    }

    /**
     * Resets and clears the data object.
     */
    @Override
    public void clearChart() {
        mData.clear();
    }

    /**
     * On touch event boolean
     *
     * @param component  component
     * @param touchEvent touch event
     * @return the boolean
     */
    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
            simulateClick();
            return true;
        } else {
            return false;
        }
    }

    /**
     * This is the main entry point after the graph has been inflated. Used to initialize the graph
     * and its corresponding members.
     */
    @Override
    protected void initializeGraph() {
        super.initializeGraph();
        mData = new ArrayList<>();
        mValuePaint = new Paint(mLegendPaint);
    }

    /**
     * Should be called after new data is inserted. Will be automatically called, when the view dimensions
     * has changed.
     */
    @Override
    protected void onDataChanged() {
        calculateBarPositions(mData.size());
        super.onDataChanged();
    }

    /**
     * Calculates the bar boundaries based on the bar width and bar margin.
     *
     * @param _Width  Calculated bar width
     * @param _Margin Calculated bar margin
     */
    protected void calculateBounds(float _Width, float _Margin) {
        float maxValue = 0;
        int last = 0;
        float maxLegendWidth = 0;

        if (mUseMaximumValue) {
            maxValue = mMaximumValue;
        } else {
            for (BarModel model : mData) {
                if (model.getValue() > maxValue) {
                    maxValue = model.getValue();
                }
            }
        }

        if (mShowBarLabel) {
            float measuredText;
            for (BarModel model : mData) {
                measuredText = mValuePaint.measureText(model.getLegendLabel());
                if (maxLegendWidth < measuredText) {
                    maxLegendWidth = measuredText;
                }
            }
        }

        float legendWidthDistance = mShowBarLabel ? maxLegendWidth + mValueDistance : 0;
        float widthMultiplier = (mGraphWidth - legendWidthDistance) / maxValue;

        for (BarModel model : mData) {
            float width = model.getValue() * widthMultiplier;
            last += _Margin / 2;
            model.setBarBounds(new RectFloat(0, last, width, last + _Width));
            model.setLegendBounds(new RectFloat(last, 0, last + _Width, mLegendHeight));
            last += _Width + (_Margin / 2);
        }

        Utils.calculateLegendInformation(getContext(), mData, 0, mContentRect.getWidth(), mLegendPaint);
        mMaxFontHeight = Utils.calculateMaxTextHeight(mValuePaint, "190");
    }

    /**
     * Callback method for drawing the bars in the child classes.
     *
     * @param _Canvas The canvas object of the graph view.
     */
    protected void drawBars(Canvas _Canvas) {
        RectFloat bounds;
        String valueString;
        float animatedRightOffset;

        for (BarModel model : mData) {
            bounds = model.getBarBounds();
            valueString = Utils.getFloatString(model.getValue(), mShowDecimal) + mValueUnit;
            animatedRightOffset = bounds.right * mRevealValue;

            mGraphPaint.setColor(new Color(model.getColor()));

            _Canvas.drawRect(new RectFloat(bounds.left,
                    bounds.top,
                    bounds.right * mRevealValue,
                    bounds.bottom), mGraphPaint);

            if (mShowValues && animatedRightOffset > mValuePaint.measureText(valueString)) {
                mValuePaint.setColor(new Color(mLegendColor));
                _Canvas.drawText(mValuePaint,
                        valueString,
                        bounds.left + mValueDistance,
                        bounds.getCenter().getPointY() + (mMaxFontHeight / 2)

                );
            }

            if (mShowBarLabel) {
                mValuePaint.setColor(new Color(mBarLabelColor));
                _Canvas.drawText(mValuePaint,
                        model.getLegendLabel(),
                        animatedRightOffset + mValueDistance,
                        bounds.getCenter().getPointY() + (mMaxFontHeight / 2)

                );
            }
        }
    }

    /**
     * Returns the list of data sets which hold the information about the legend boundaries and text.
     *
     * @return List of BaseModel data sets.
     */
    @Override
    protected List<? extends BaseModel> getLegendData() {
        return mData;
    }

    /**
     * Get bar bounds list
     *
     * @return the list
     */
    @Override
    protected List<RectFloat> getBarBounds() {
        ArrayList<RectFloat> bounds = new ArrayList<RectFloat>();
        for (BarModel model : mData) {
            bounds.add(model.getBarBounds());
        }
        return bounds;
    }
}
