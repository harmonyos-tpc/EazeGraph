/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.eazegraph.lib.utils;

import org.eazegraph.lib.gesture.ScaleGestureDetector;

/**
 * A utility class for using in a backward-compatible
 * fashion.
 */
public class ScaleGestureDetectorCompat {
    /**
     * Disallow instantiation.
     */
    private ScaleGestureDetectorCompat() {
    }


    /**
     * Get current span x float
     *
     * @param scaleGestureDetector scale gesture detector
     * @return the float
     */
    public static float getCurrentSpanX(ScaleGestureDetector scaleGestureDetector) {
        return scaleGestureDetector.getCurrentSpanX();
    }


    /**
     * Get current span y float
     *
     * @param scaleGestureDetector scale gesture detector
     * @return the float
     */
    public static float getCurrentSpanY(ScaleGestureDetector scaleGestureDetector) {
        return scaleGestureDetector.getCurrentSpanY();
    }


    /**
     * Get previous span x float
     *
     * @param scaleGestureDetector scale gesture detector
     * @return the float
     */
    public static float getPreviousSpanX(ScaleGestureDetector scaleGestureDetector) {
        return scaleGestureDetector.getPreviousSpanX();
    }


    /**
     * Get previous span y float
     *
     * @param scaleGestureDetector scale gesture detector
     * @return the float
     */
    public static float getPreviousSpanY(ScaleGestureDetector scaleGestureDetector) {
        return scaleGestureDetector.getPreviousSpanY();
    }
}
