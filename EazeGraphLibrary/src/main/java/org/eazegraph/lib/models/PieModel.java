/**
 * Copyright (C) 2014 Paul Cech
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.eazegraph.lib.models;



/**
 * Model for the {@link PieChart}
 */
public class PieModel extends BaseModel implements Comparable {

    /**
     * Value of the Pie Slice
     */
    private float mValue;
    /**
     * The color in which the pie slice will be drawn.
     */
    private int mColor;
    /**
     * The highlighted mColor value.
     */
    private int mHighlightedColor;
    /**
     * Start angle in the PieChart
     */
    private int mStartAngle;
    /**
     * End angle in the PieChart
     */
    private int mEndAngle;

    /**
     * Pie model
     *
     * @param _legendLabel legend label
     * @param _value       value
     * @param _color       color
     */
    public PieModel(String _legendLabel, float _value, int _color) {
        super(_legendLabel);
        mValue = _value;
        mColor = _color;
    }

    /**
     * Pie model
     *
     * @param _value value
     * @param _color color
     */
    public PieModel(float _value, int _color) {
        super("");
        mValue = _value;
        mColor = _color;
    }

    /**
     * Pie model
     */
    public PieModel() {
    }

    /**
     * Get value float
     *
     * @return the float
     */
    public float getValue() {
        return mValue;
    }

    /**
     * Set value *
     *
     * @param _Value value
     */
    public void setValue(float _Value) {
        mValue = _Value;
    }

    /**
     * Get color int
     *
     * @return the int
     */
    public int getColor() {
        return mColor;
    }

    /**
     * Set color *
     *
     * @param _Color color
     */
    public void setColor(int _Color) {
        mColor = _Color;
    }

    /**
     * Get highlighted color int
     *
     * @return the int
     */
    public int getHighlightedColor() {
        return mHighlightedColor;
    }

    /**
     * Set highlighted color *
     *
     * @param _HighlightedColor highlighted color
     */
    public void setHighlightedColor(int _HighlightedColor) {
        mHighlightedColor = _HighlightedColor;
    }

    /**
     * Get start angle int
     *
     * @return the int
     */
    public int getStartAngle() {
        return mStartAngle;
    }

    /**
     * Set start angle *
     *
     * @param _StartAngle start angle
     */
    public void setStartAngle(int _StartAngle) {
        mStartAngle = _StartAngle;
    }

    /**
     * Get end angle int
     *
     * @return the int
     */
    public int getEndAngle() {
        return mEndAngle;
    }

    /**
     * Set end angle *
     *
     * @param _EndAngle end angle
     */
    public void setEndAngle(int _EndAngle) {
        mEndAngle = _EndAngle;
    }

    /**
     * Compare to int
     *
     * @param object object
     * @return the int
     */
    @Override
    public int compareTo(Object object) {
        PieModel pie = (PieModel) object;
        if (this.mValue > pie.getValue()) {
            return 1;
        } else if (this.mValue == pie.getValue()) {
            return 0;
        } else {
            return -1;
        }
    }

}
