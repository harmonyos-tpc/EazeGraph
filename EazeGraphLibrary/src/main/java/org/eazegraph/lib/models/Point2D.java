/**
 * Copyright (C) 2014 Paul Cech
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.eazegraph.lib.models;

/**
 * A simple wrapper class for the representation of coordinates.
 */
public class Point2D {

    /**
     * The constant M x
     */
    private float mX;
    /**
     * The constant M y
     */
    private float mY;

    /**
     * Point 2 d
     *
     * @param _x x
     * @param _y y
     */
    public Point2D(float _x, float _y) {
        mX = _x;
        mY = _y;
    }

    /**
     * Point 2 d
     */
    public Point2D() {
    }

    /**
     * Get x float
     *
     * @return the float
     */
    public float getX() {
        return mX;
    }

    /**
     * Set x *
     *
     * @param _x x
     */
    public void setX(float _x) {
        mX = _x;
    }

    /**
     * Get y float
     *
     * @return the float
     */
    public float getY() {
        return mY;
    }

    /**
     * Set y *
     *
     * @param _y y
     */
    public void setY(float _y) {
        mY = _y;
    }

    /**
     * Get float array float [ ]
     *
     * @return the float [ ]
     */
    public float[] getFloatArray() {
        return new float[]{mX, mY};
    }

    /**
     * To string string
     *
     * @return the string
     */
    @Override
    public String toString() {
        return "Point2D{" +
                "mX=" + mX +
                ", mY=" + mY +
                '}';
    }
}
