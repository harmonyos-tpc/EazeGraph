/**
 * Copyright (C) 2014 Paul Cech
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.eazegraph.lib.models;

/**
 * Model for the {@link ValueLineSeries}.
 */
public class ValueLinePoint extends BaseModel implements Comparable {

    /**
     * Value of the Point.
     */
    private float mValue;
    /**
     * The coordinates for the chart. These are calculated dynamically.
     */
    private Point2D mCoordinates;

    /**
     * Value line point
     *
     * @param _value value
     */
    public ValueLinePoint(float _value) {
        super("" + _value);
        mValue = _value;
    }

    /**
     * Value line point
     *
     * @param _legendLabel legend label
     * @param _value       value
     */
    public ValueLinePoint(String _legendLabel, float _value) {
        super(_legendLabel);
        mValue = _value;
    }

    /**
     * Get value float
     *
     * @return the float
     */
    public float getValue() {
        return mValue;
    }

    /**
     * Set value *
     *
     * @param _value value
     */
    public void setValue(float _value) {
        mValue = _value;
    }

    /**
     * Get coordinates point 2 d
     *
     * @return the point 2 d
     */
    public Point2D getCoordinates() {
        return mCoordinates;
    }

    /**
     * Set coordinates *
     *
     * @param _coordinates coordinates
     */
    public void setCoordinates(Point2D _coordinates) {
        mCoordinates = _coordinates;
    }

    /**
     * Compare to int
     *
     * @param object object
     * @return the int
     */
    @Override
    public int compareTo(Object object) {
        ValueLinePoint point = (ValueLinePoint) object;
        if (this.mValue > point.getValue()) {
            return 1;
        } else if (this.mValue == point.getValue()) {
            return 0;
        } else {
            return -1;
        }
    }

    /**
     * To string string
     *
     * @return the string
     */
    @Override
    public String toString() {
        return "ValueLinePoint{" +
                "mValue=" + mValue +
                ", mCoordinates=" + mCoordinates +
                '}';
    }
}
