/**
 * Copyright (C) 2014 Paul Cech
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.eazegraph.lib.models;

import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;


/**
 * Model for the {@link BarChart}
 */
public class BarModel extends BaseModel implements Comparable {

    /**
     * Value of the bar.
     */
    private float mValue;
    /**
     * Color in which the bar will be drawn.
     */
    private int mColor;
    /**
     * Bar boundaries.
     */
    private RectFloat mBarBounds;
    /**
     * The constant M show value
     */
    private boolean mShowValue = false;
    /**
     * The constant M value bounds
     */
    private Rect mValueBounds = new Rect();

    /**
     * Bar model
     *
     * @param _legendLabel legend label
     * @param _value       value
     * @param _color       color
     */
    public BarModel(String _legendLabel, float _value, int _color) {
        super(_legendLabel);
        mValue = _value;
        mColor = _color;
    }

    /**
     * Bar model
     *
     * @param _value value
     * @param _color color
     */
    public BarModel(float _value, int _color) {
        super("" + _value);
        mValue = _value;
        mColor = _color;
    }

    /**
     * Bar model
     *
     * @param _value value
     */
    public BarModel(float _value) {
        super("" + _value);
        mValue = _value;
        mColor = 0xFFFF0000;
    }

    /**
     * Get value float
     *
     * @return the float
     */
    public float getValue() {
        return mValue;
    }

    /**
     * Set value *
     *
     * @param _value value
     */
    public void setValue(float _value) {
        mValue = _value;
    }

    /**
     * Get color int
     *
     * @return the int
     */
    public int getColor() {
        return mColor;
    }

    /**
     * Set color *
     *
     * @param _color color
     */
    public void setColor(int _color) {
        mColor = _color;
    }

    /**
     * Get bar bounds rect float
     *
     * @return the rect float
     */
    public RectFloat getBarBounds() {
        return mBarBounds;
    }

    /**
     * Set bar bounds *
     *
     * @param _bounds bounds
     */
    public void setBarBounds(RectFloat _bounds) {
        mBarBounds = _bounds;
    }

    /**
     * Is show value boolean
     *
     * @return the boolean
     */
    public boolean isShowValue() {
        return mShowValue;
    }

    /**
     * Set show value *
     *
     * @param _showValue show value
     */
    public void setShowValue(boolean _showValue) {
        mShowValue = _showValue;
    }

    /**
     * Get value bounds rect
     *
     * @return the rect
     */
    public Rect getValueBounds() {
        return mValueBounds;
    }

    /**
     * Set value bounds *
     *
     * @param _valueBounds value bounds
     */
    public void setValueBounds(Rect _valueBounds) {
        mValueBounds = _valueBounds;
    }

    /**
     * Compare to int
     *
     * @param object object
     * @return the int
     */
    @Override
    public int compareTo(Object object) {
        BarModel bar = (BarModel) object;
        if (this.mValue > bar.getValue()) {
            return 1;
        } else if (this.mValue == bar.getValue()) {
            return 0;
        } else {
            return -1;
        }
    }
}