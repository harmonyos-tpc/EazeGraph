/**
 * Copyright (C) 2014 Paul Cech
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.eazegraph.lib.models;

import ohos.agp.utils.RectFloat;
import java.util.ArrayList;
import java.util.List;

/**
 * Model for the {@link StackedBarChart}.
 * This is a simple Wrapper class for various {@link BarModel}
 */
public class StackedBarModel extends BaseModel {

    /**
     * Bars which are in the StackedBar.
     */
    List<BarModel> mBars;

    /**
     * Stacked bar model
     */
    public StackedBarModel() {
        super("Unset");
        mBars = new ArrayList<BarModel>();
    }

    /**
     * Stacked bar model
     *
     * @param _legendLabel legend label
     */
    public StackedBarModel(String _legendLabel) {
        super(_legendLabel);
        mBars = new ArrayList<BarModel>();
    }

    /**
     * Stacked bar model
     *
     * @param _bars bars
     */
    public StackedBarModel(List<BarModel> _bars) {
        super("Unset");
        mBars = _bars;
    }

    /**
     * Stacked bar model
     *
     * @param _legendLabel legend label
     * @param _bars        bars
     */
    public StackedBarModel(String _legendLabel, List<BarModel> _bars) {
        super(_legendLabel);
        mBars = _bars;
    }

    /**
     * Get bars list
     *
     * @return the list
     */
    public List<BarModel> getBars() {
        return mBars;
    }

    /**
     * Set bars *
     *
     * @param _bars bars
     */
    public void setBars(List<BarModel> _bars) {
        mBars = _bars;
    }

    /**
     * Add bar *
     *
     * @param _bar bar
     */
    public void addBar(BarModel _bar) {
        mBars.add(_bar);
    }

    /**
     * Get bounds rect float
     *
     * @return the rect float
     */
    public RectFloat getBounds() {
        RectFloat bounds = new RectFloat();
        if (!mBars.isEmpty()) {
            // get bounds from complete StackedBar
            bounds = new RectFloat(mBars.get(0).getBarBounds().left,
                    mBars.get(0).getBarBounds().top,
                    mBars.get(mBars.size() - 1).getBarBounds().right,
                    mBars.get(mBars.size() - 1).getBarBounds().bottom);
        }
        return bounds;
    }
}
