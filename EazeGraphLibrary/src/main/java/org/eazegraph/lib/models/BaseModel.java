/**
 * Copyright (C) 2014 Paul Cech
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.eazegraph.lib.models;

import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;

/**
 * The BaseModel is the parent model of every chart model. It basically only holds the information
 * about the legend labels of the childs value.
 */
public abstract class BaseModel {

    /**
     * Label value
     */
    protected String mLegendLabel;
    /**
     * Indicates whether the label should be shown or not.
     */
    protected boolean mShowLabel;
    /**
     * X-coordinate of the label.
     */
    private int mLegendLabelPosition;
    /**
     * Boundaries of the label
     */
    private RectFloat mLegendBounds;
    /**
     * Boundaries of the legend labels value
     */
    private Rect mTextBounds;
    /**
     * Indicates if the label should be ignored, when the boundaries are calculated.
     */
    private boolean mIgnore = false;

    /**
     * Base model
     *
     * @param _legendLabel legend label
     */
    protected BaseModel(String _legendLabel) {
        mLegendLabel = _legendLabel;
    }

    /**
     * Base model
     */
    protected BaseModel() {
    }

    /**
     * Get legend label string
     *
     * @return the string
     */
    public String getLegendLabel() {
        return mLegendLabel;
    }

    /**
     * Set legend label *
     *
     * @param _LegendLabel legend label
     */
    public void setLegendLabel(String _LegendLabel) {
        mLegendLabel = _LegendLabel;
    }

    /**
     * Can show label boolean
     *
     * @return the boolean
     */
    public boolean canShowLabel() {
        return mShowLabel;
    }

    /**
     * Set show label *
     *
     * @param _showLabel show label
     */
    public void setShowLabel(boolean _showLabel) {
        mShowLabel = _showLabel;
    }

    /**
     * Get legend label position int
     *
     * @return the int
     */
    public int getLegendLabelPosition() {
        return mLegendLabelPosition;
    }

    /**
     * Set legend label position *
     *
     * @param _legendLabelPosition legend label position
     */
    public void setLegendLabelPosition(int _legendLabelPosition) {
        mLegendLabelPosition = _legendLabelPosition;
    }

    /**
     * Get legend bounds rect float
     *
     * @return the rect float
     */
    public RectFloat getLegendBounds() {
        return mLegendBounds;
    }

    /**
     * Set legend bounds *
     *
     * @param _legendBounds legend bounds
     */
    public void setLegendBounds(RectFloat _legendBounds) {
        mLegendBounds = _legendBounds;
    }

    /**
     * Get text bounds rect
     *
     * @return the rect
     */
    public Rect getTextBounds() {
        return mTextBounds;
    }

    /**
     * Set text bounds *
     *
     * @param _textBounds text bounds
     */
    public void setTextBounds(Rect _textBounds) {
        mTextBounds = _textBounds;
    }

    /**
     * Is ignore boolean
     *
     * @return the boolean
     */
    public boolean isIgnore() {
        return mIgnore;
    }

    /**
     * Set ignore *
     *
     * @param _ignore ignore
     */
    public void setIgnore(boolean _ignore) {
        mIgnore = _ignore;
    }
}
