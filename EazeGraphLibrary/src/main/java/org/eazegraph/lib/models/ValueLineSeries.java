/**
 * Copyright (C) 2014 Paul Cech
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.eazegraph.lib.models;

import ohos.agp.render.Path;
import org.eazegraph.lib.charts.ValueLineChart;

import java.util.ArrayList;
import java.util.List;

/**
 * Model for the {@link ValueLineChart}
 */
public class ValueLineSeries {

    /**
     * The list of points, which will be concatenated as a Path.
     */
    private List<ValueLinePoint> mSeries;
    /**
     * The generated Path based on the mSeries points.
     */
    private Path mPath;
    /**
     * The color of the path.
     */
    private int mColor;
    /**
     * Indicates the offset between each point in the series. This is calculated dynamically.
     */
    private float mWidthOffset;

    /**
     * Value line series
     *
     * @param _series series
     */
    public ValueLineSeries(List<ValueLinePoint> _series) {
        mSeries = _series;
        mPath = new Path();
    }

    /**
     * Value line series
     */
    public ValueLineSeries() {
        mSeries = new ArrayList<ValueLinePoint>();
        mPath = new Path();
    }

    /**
     * Add point *
     *
     * @param _valueLinePoint value line point
     */
    public void addPoint(ValueLinePoint _valueLinePoint) {
        mSeries.add(_valueLinePoint);
    }

    /**
     * Get series list
     *
     * @return the list
     */
    public List<ValueLinePoint> getSeries() {
        return mSeries;
    }

    /**
     * Set series *
     *
     * @param _series series
     */
    public void setSeries(List<ValueLinePoint> _series) {
        mSeries = _series;
    }

    /**
     * Get color int
     *
     * @return the int
     */
    public int getColor() {
        return mColor;
    }

    /**
     * Set color *
     *
     * @param _color color
     */
    public void setColor(int _color) {
        mColor = _color;
    }

    /**
     * Get path path
     *
     * @return the path
     */
    public Path getPath() {
        return mPath;
    }

    /**
     * Set path *
     *
     * @param _path path
     */
    public void setPath(Path _path) {
        mPath = _path;
    }

    /**
     * Get width offset float
     *
     * @return the float
     */
    public float getWidthOffset() {
        return mWidthOffset;
    }

    /**
     * Set width offset *
     *
     * @param _widthOffset width offset
     */
    public void setWidthOffset(float _widthOffset) {
        mWidthOffset = _widthOffset;
    }

    /**
     * To string string
     *
     * @return the string
     */
    @Override
    public String toString() {
        return "ValueLineSeries{" +
                "mSeries=" + mSeries +
                ", mPath=" + mPath +
                ", mColor=" + mColor +
                ", mWidthOffset=" + mWidthOffset +
                '}';
    }
}
