/**
 * Copyright (C) 2014 Paul Cech
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.eazegraph.lib.models;

import ohos.app.Context;
import org.eazegraph.lib.utils.Utils;

/**
 * Created by paul on 13.08.14.
 */
public class StandardValue {

    /**
     * * The constant DEF_STANDARD_VALUE_INDICATOR_STROKE
     */
    public static final float DEF_STANDARD_VALUE_INDICATOR_STROKE = 2f;
    /**
     * * The constant DEF_STANDARD_VALUE_COLOR
     */
    public static final int DEF_STANDARD_VALUE_COLOR = 0xFF00FF00;
    /**
     * The constant M color
     */
    private int mColor;
    /**
     * The constant M value
     */
    private float mValue;
    /**
     * The constant M y
     */
    private int mY;
    /**
     * The constant M stroke
     */
    private float mStroke;

    /**
     * Standard value
     *
     * @param _color  color
     * @param _value  value
     * @param _stroke The stroke height in dp
     */
    public StandardValue(Context context, int _color, float _value, float _stroke) {
        mColor = _color;
        mValue = _value;
        mStroke = Utils.dpToPx(context, _stroke);
    }

    /**
     * Standard value
     *
     * @param _value value
     */
    public StandardValue(Context context, float _value) {
        mColor = DEF_STANDARD_VALUE_COLOR;
        mValue = _value;
        mStroke = Utils.dpToPx(context, DEF_STANDARD_VALUE_INDICATOR_STROKE);
    }

    /**
     * Get color int
     *
     * @return the int
     */
    public int getColor() {
        return mColor;
    }

    /**
     * Set color *
     *
     * @param _color color
     */
    public void setColor(int _color) {
        mColor = _color;
    }

    /**
     * Get value float
     *
     * @return the float
     */
    public float getValue() {
        return mValue;
    }

    /**
     * Set value *
     *
     * @param _value value
     */
    public void setValue(float _value) {
        mValue = _value;
    }

    /**
     * Get y int
     *
     * @return the int
     */
    public int getY() {
        return mY;
    }

    /**
     * Set y *
     *
     * @param _y y
     */
    public void setY(int _y) {
        mY = _y;
    }

    /**
     * Get stroke float
     *
     * @return the float
     */
    public float getStroke() {
        return mStroke;
    }

    /**
     * Set stroke *
     *
     * @param _stroke The stroke height in dp
     */
    public void setStroke(Context context,float _stroke) {
        mStroke = Utils.dpToPx(context, _stroke);
    }
}
